# Variables for adjusting ASG
variable "asg_min_size" {
  type    = number
  default = 1
}
variable "asg_desired_capacity" {
  type    = number
  default = 2
}
variable "asg_max_size" {
  type    = number
  default = 4
}

# Variables for modifying APP
variable "height" {
  default     = "800"
  description = "Image height in pixels."
}
variable "width" {
  default     = "800"
  description = "Image width in pixels."
}
variable "placeholder" {
  default     = "placedog.net"
  description = "Image-as-a-service URL. Some other fun ones to try are placekitten.com, placebeard.it, loremflickr.com, baconmockup.com, placeimg.com, placebear.com, placeskull.com"
}
variable "text" {
  default     = "Woof!"
  description = "Change the text above to something more meaningful"
}