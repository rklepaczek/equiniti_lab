resource "aws_launch_configuration" "sa_lab_lc" {
  name_prefix                 = "${var.prefix}-"
  image_id                    = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  security_groups             = [aws_security_group.alb_sg.id]
  associate_public_ip_address = true
  user_data_base64            = base64gzip(data.template_file.user_data.rendered)

  lifecycle {
    create_before_destroy = true
  }
}