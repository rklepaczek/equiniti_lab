resource "azurerm_virtual_network" "vmss" {
  name                = "${var.prefix}-vnet"
  address_space       = [var.address_space]
  location            = var.location
  resource_group_name = azurerm_resource_group.vmss.name
  tags                = var.tags
}