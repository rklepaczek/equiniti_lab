resource "azurerm_linux_virtual_machine_scale_set" "vmss" {
  name                            = "${var.prefix}-vmss"
  resource_group_name             = module.network.vmss_resource_group_name
  location                        = module.network.vmss_location
  sku                             = var.instance_size
  instances                       = var.number_of_instances
  admin_username                  = var.admin_user
  admin_password                  = var.admin_password
  disable_password_authentication = false
  custom_data                     = base64encode(data.template_file.script.rendered)

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  network_interface {
    name    = "${var.prefix}-vmss-nic"
    primary = true

    ip_configuration {
      name                                   = "${var.prefix}-vmss-ipconfig"
      primary                                = true
      subnet_id                              = module.network.vmss_subnet_id
      load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.vmss.id]
    }
    network_security_group_id = azurerm_network_security_group.vmss.id
  }

  boot_diagnostics {
    storage_account_uri = null
  }

  lifecycle {
    create_before_destroy = true
  }
}

